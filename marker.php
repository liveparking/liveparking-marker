<?php

// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

if (isset($_GET['text'])) {
  $text = urldecode($_GET['text']);
  $text = substr($text, 0, 4); // Max. 4 digits
} else {
  $text = '';
}

if (isset($_GET['hex'])) {
  $hex = urlencode($_GET['hex']);
} else {
  $hex = '70bf3d';
}

$img = imagecreate(200, 200);

// Make bg transparent
$background = imagecolorallocatealpha($img, 0, 0, 0, 127);
imagefill($img, 0, 0, $background);

$outer = imagecolorallocatealpha($img, 255,   255,   255, 0);
$inner = hexColorAllocate($img, $hex);
$textColor = imagecolorallocate($img, 255, 255, 255);
$font = './assets/BebasNeueProBold.otf';

imagefilledellipse($img, 100, 100, 200, 200, $outer);
imagefilledellipse($img, 100, 100, 180, 180, $inner);

$textLength = strlen($text);

switch ($textLength) {
  case 4:
    $fontSize = 76;
    $xPos = 19; // lower = left, higher = right
    $yPos = 134; // lower = up, higher = down
    break;
  case 3:
    $fontSize = 90;
    $xPos = 19; // lower = left, higher = right
    $yPos = 142; // lower = up, higher = down
    break;
  default:
    $fontSize = 100;
    $xPos = 25; // lower = left, higher = right
    $yPos = 147; // lower = up, higher = down
}

// Center the text
$dimensions = getTextDimensions($fontSize, $font, $text);
$xPos = imagesx($img) / 2 - $dimensions["width"] / 2 - 2;

imagettftext($img, $fontSize, 0, $xPos, $yPos, $textColor, $font, $text);

header('Content-Type: image/png');
imagePNG($img);
imagedestroy($img);


function hexColorAllocate($im, $hex)
{
  $hex = ltrim($hex, '#');
  $a = hexdec(substr($hex, 0, 2));
  $b = hexdec(substr($hex, 2, 2));
  $c = hexdec(substr($hex, 4, 2));
  return imagecolorallocate($im, $a, $b, $c);
}

/**
 * It returns the dimensions of the text to print with the given 
 * size and font.
 * 
 * @return array containing the width and height (width,heigh) of the text to print.
 * @access public
 */
function getTextDimensions($fontSize, $fontFile, $text)
{
  $dimensions = imagettfbbox($fontSize, 0, $fontFile, $text);

  $minX = min(array($dimensions[0], $dimensions[2], $dimensions[4], $dimensions[6]));
  $maxX = max(array($dimensions[0], $dimensions[2], $dimensions[4], $dimensions[6]));

  $minY = min(array($dimensions[1], $dimensions[3], $dimensions[5], $dimensions[7]));
  $maxY = max(array($dimensions[1], $dimensions[3], $dimensions[5], $dimensions[7]));

  return array(
    'width' => $maxX - $minX,
    'heigh' => $maxY - $minY
  );
}
